package ru.arduinohub.bt_manipulator;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BluetoothAdapter bluetooth= BluetoothAdapter.getDefaultAdapter();

        if(bluetooth!=null)
        {
            // С Bluetooth все в порядке.
            if (bluetooth.isEnabled()) {
                // Bluetooth включен. Работаем.
                // String mydeviceaddress= bluetooth.getAddress();
                // String mydevicename= bluetooth.getName();
                // status= mydevicename+" : "+ mydeviceaddress;
            }
            else
            {
                // Bluetooth выключен. Предложим пользователю включить его.
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                // startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }
}
